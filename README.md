# Antenna Simulation

Runs antenna simulation using [Finite-Difference Time-Domain method](https://en.wikipedia.org/wiki/Finite-difference_time-domain_method)
solutions to Maxwell's equations.

# Installation

On Debian systems:

```
$ sudo apt install python3-meep h5utils
$ virtualenv -p python3 --use-system-site-packages pyenv
$ pyenv/bin/pip install -r requirements.txt
$ pyenv/bin/python -m antennasimulation
```
