###########################################################################
# AntennaSimulation is Copyright (C) 2020 Kyle Robbertze <kyle@paddatrapper.com>
#
# AntennaSimulation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# AntennaSimulation is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AntennaSimulation. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import meep as mp
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    cell = mp.Vector3(16, 8, 0)
    geometry = [mp.Block(mp.Vector3(mp.inf, 1, mp.inf),
                         center=mp.Vector3(),
                         material=mp.Medium(epsilon=12))]
    sources = [mp.Source(mp.ContinuousSource(frequency=0.15),
                         component=mp.Ez,
                         center=mp.Vector3(-7, 0))]
    pml_layers = [mp.PML(1.0)]
    resolution = 10
    sim = mp.Simulation(cell_size=cell,
                        boundary_layers=pml_layers,
                        geometry=geometry,
                        sources=sources,
                        resolution=resolution)
    sim.run(until=200)

    eps_data = sim.get_array(center=mp.Vector3(), size=cell, component=mp.Dielectric)
    ez_data = sim.get_array(center=mp.Vector3(), size=cell, component=mp.Ez)
    plt.figure()
    plt.imshow(eps_data.transpose(), interpolation='spline36', cmap='binary')
    plt.imshow(ez_data.transpose(), interpolation='spline36', cmap='RdBu', alpha=0.9)
    plt.axis('off')
    plt.show()
