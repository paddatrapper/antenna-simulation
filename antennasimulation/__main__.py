###########################################################################
# AntennaSimulation is Copyright (C) 2020 Kyle Robbertze <kyle@paddatrapper.com>
#
# AntennaSimulation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# AntennaSimulation is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AntennaSimulation. If not, see <http://www.gnu.org/licenses/>.
#
# Following https://meep.readthedocs.io/en/latest/Python_Tutorials/Near_to_Far_Field_Spectra/
###########################################################################
import meep as mp
from meep.materials import Cu
import math
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    resolution = 50e-4

    sxy = 4e6
    dpml = 1e3
    cell = mp.Vector3(sxy + 2 * dpml, sxy + 2 * dpml)

    pml_layers = [mp.PML(dpml)]

    fcen = 140e6/3e8
    df = 50/3e8
    src_cmpt = mp.Ez
    sources = [mp.Source(src=mp.GaussianSource(fcen, fwidth=df),
                         center=mp.Vector3(),
                         component=src_cmpt)]
    geometry = [
        mp.Block(mp.Vector3(1e3, 2e6),
                 center=mp.Vector3(),
                 material=Cu),
        mp.Block(mp.Vector3(1e3, 2e6),
                 center=mp.Vector3(-1e6,0),
                 material=Cu),
        mp.Block(mp.Vector3(1e3, 2e6),
                 center=mp.Vector3(1e6,0),
                 material=Cu),
    ]

    if src_cmpt == mp.Ex:
        symmetries = [mp.Mirror(mp.X, phase=-1),
                      mp.Mirror(mp.Y, phase=1)]
    elif src_cmpt == mp.Ey:
        symmetries = [mp.Mirror(mp.X, phase=1),
                      mp.Mirror(mp.Y, phase=-1)]
    elif src_cmpt == mp.Ez:
        symmetries = [mp.Mirror(mp.X, phase=1),
                      mp.Mirror(mp.Y, phase=1)]

    sim = mp.Simulation(cell_size=cell,
                        resolution=resolution,
                        sources=sources,
                        geometry=geometry,
                        symmetries=symmetries,
                        boundary_layers=pml_layers)
    nearfield_box = sim.add_near2far(fcen, 0, 1,
                                     mp.Near2FarRegion(center=mp.Vector3(0, 0.5 * sxy),
                                                       size=mp.Vector3(sxy, 0),
                                                       weight=1),
                                     mp.Near2FarRegion(center=mp.Vector3(0, -0.5 * sxy),
                                                       size=mp.Vector3(sxy, 0),
                                                       weight=-1),
                                     mp.Near2FarRegion(center=mp.Vector3(0.5 * sxy, 0),
                                                       size=mp.Vector3(0, sxy),
                                                       weight=1),
                                     mp.Near2FarRegion(center=mp.Vector3(-0.5 * sxy, 0),
                                                       size=mp.Vector3(0, sxy),
                                                       weight=-1))

    flux_box = sim.add_flux(fcen, 0, 1,
                            mp.FluxRegion(center=mp.Vector3(0, 0.5 * sxy),
                                          size=mp.Vector3(sxy, 0),
                                          weight=1),
                            mp.FluxRegion(center=mp.Vector3(0, -0.5 * sxy),
                                          size=mp.Vector3(sxy, 0),
                                          weight=-1),
                            mp.FluxRegion(center=mp.Vector3(0.5 * sxy, 0),
                                          size=mp.Vector3(0, sxy),
                                          weight=1),
                            mp.FluxRegion(center=mp.Vector3(-0.5 * sxy, 0),
                                          size=mp.Vector3(0, sxy),
                                          weight=-1))

    sim.run(until_after_sources=mp.stop_when_fields_decayed(50, src_cmpt, mp.Vector3(), 1e-5))

    near_flux = mp.get_fluxes(flux_box)[0]

    r = 1000 / fcen
    res_ff = 1
    far_flux_box = (nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=r),
                                                       size=mp.Vector3(2 * r)), res_ff)[0]
                    - nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=-r),
                                                         size=mp.Vector3(2 * r)), res_ff)[0]
                    + nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(r),
                                                         size=mp.Vector3(y=2 * r)), res_ff)[0]
                    - nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(-r),
                                                         size=mp.Vector3(y=2 * r)), res_ff)[0])

    npts = 100
    angles = 2 * math.pi / npts * np.arange(npts)

    E = np.zeros((npts, 3), dtype=np.complex128)
    H = np.zeros((npts, 3), dtype=np.complex128)
    for n in range(npts):
        ff = sim.get_farfield(nearfield_box,
                              mp.Vector3(r * math.cos(angles[n]),
                                         r * math.sin(angles[n])))
        E[n,:] = [np.conj(ff[j]) for j in range(3)]
        H[n,:] = [ff[j + 3] for j in range(3)]

    Px = np.real(E[:,1] * H[:,2] - E[:,2] * H[:,1])
    Py = np.real(E[:,2] * H[:,0] - E[:,0] * H[:,2])
    Pr = np.sqrt(np.square(Px) + np.square(Py))

    far_flux_circle = np.sum(Pr) * 2 * np.pi * r / len(Pr)

    print("flux:, {:.6f}, {:.6f}, {:.6f}".format(near_flux,far_flux_box,far_flux_circle))

    ax = plt.subplot(111, projection='polar')
    ax.plot(angles,Pr/max(Pr),'b-')
    ax.set_rmax(1)
    ax.set_rticks([0,0.5,1])
    ax.grid(True)
    ax.set_rlabel_position(22)
    plt.show()
