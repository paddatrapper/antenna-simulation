###########################################################################
# AntennaSimulation is Copyright (C) 2020 Kyle Robbertze <kyle@paddatrapper.com>
#
# AntennaSimulation is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# AntennaSimulation is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AntennaSimulation. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import meep as mp
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    cell = mp.Vector3(16, 16, 0)
    geometry = [
        mp.Block(mp.Vector3(12, 1, mp.inf),
                 center=mp.Vector3(-2.5,3.5),
                 material=mp.Medium(epsilon=12)),
        mp.Block(mp.Vector3(1, 12, mp.inf),
                 center=mp.Vector3(3.5, -2),
                 material=mp.Medium(epsilon=12)),
    ]
    pml_layers = [mp.PML(1.0)]
    resolution = 10
    sources = [mp.Source(mp.ContinuousSource(wavelength=2.33*(11**0.5), width=20),
                         component=mp.Ez,
                         center=mp.Vector3(-7, 3.5),
                         size=mp.Vector3(0,1))]

    sim = mp.Simulation(cell_size=cell,
                        boundary_layers=pml_layers,
                        geometry=geometry,
                        sources=sources,
                        resolution=resolution)

    vals = []

    def get_slice(sim):
        vals.append(sim.get_array(center=mp.Vector3(0,3.5),
                                  size=mp.Vector3(16, 0),
                                  component=mp.Ez))

    sim.run(mp.at_beginning(mp.output_epsilon),
            mp.at_every(0.6, mp.output_png(mp.Ez, '-Zc dkbluered')),
            until=200)
